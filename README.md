# 🐭 Site du BDE - ISIMA

## 💻 Installer le projet
* Créer une base de données `ZZ_BDD`
* Faire les migrations `make migrate`
* Faire le seeding pour les données bidons `make seed`
* Lancer mysql si il n'est pas lancé `make mysqlStart`
* 🌍 Lancer le server `make start` (ou `make dev` pour le mode de développement)

## Notes
* Le nom de la base de données et les identifiant de connections peut être modifiés dans `config/config.php`
* En developpement le serveur de mail utiliser est [maildev](https://danfarrelly.nyc/MailDev/)
* Nécessite PHP 7.1 (avec php7.1-gd)


