<?php

use Framework\Middleware\CsrfMiddleware;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Framework\Router\RouterFactory;
use Framework\Router\RouterTwigExtension;
use Framework\Renderer\TwigRendererFactory;
use Framework\Session\PHPSession;
use Framework\Session\SessionInterface;
use Framework\Twig\CsrfExtension;
use Framework\Twig\DeleteButtonExtension;
use Framework\Twig\FlashExtension;
use Framework\Twig\FormExtension;
use Framework\Twig\ImageExtension;
use Framework\Twig\ModalExtension;
use Framework\Twig\NavExtension;
use Framework\Twig\PagerFantaExtension;
use Framework\Twig\TextExtension;
use Framework\Twig\TimeExtension;

/**
 * Tableau permettant de résoudre les injections de dépendances
 * Contient la configuration standards de l'application
 * peut être surcharger par les configurations des modules
 */
return [
    'env' => \DI\env('ENV', 'production'),
    'database.host' => 'localhost',
    'database.username' => 'root',
    'database.password' => 'root',
    'database.name' => 'ZZ_BDD',
    'views.path' => dirname(__DIR__) . '/views',
    'twig.extensions' => [
        \DI\get(RouterTwigExtension::class),
        \DI\get(PagerFantaExtension::class),
        \DI\get(TextExtension::class),
        \DI\get(TimeExtension::class),
        \DI\get(FlashExtension::class),
        \DI\get(FormExtension::class),
        \DI\get(CsrfExtension::class),
        \DI\get(NavExtension::class),
        \DI\get(DeleteButtonExtension::class),
        \DI\get(ModalExtension::class),
        \DI\get(ImageExtension::class),
    ],
    SessionInterface::class => \DI\create(PHPSession::class),
    CsrfMiddleware::class => \DI\autowire()->constructor(\DI\get(SessionInterface::class)),
    Router::class => \DI\factory(RouterFactory::class),
    RendererInterface::class => \DI\factory(TwigRendererFactory::class),
    \PDO::class => function (Psr\Container\ContainerInterface $c) {
        return new PDO(
            'mysql:host=' . $c->get('database.host') . ';dbname=' . $c->get('database.name'),
            $c->get('database.username'),
            $c->get('database.password'),
            [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]
        );
    },

    // MAILER
    //'mail.to' => 'admin@admin.fr',
    'mail.from' => 'noreplay@bde.fr',
    Swift_Mailer::class => \DI\factory(\Framework\SwiftMailerFactory::class)
];
