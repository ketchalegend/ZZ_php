<?php

namespace App\Account\Actions;

use App\Account\Mailer\ConfirmationMailer;
use App\Account\Table\UserTable;
use App\Account\User;
use Framework\Actions\CrudAction;
use Framework\Database\Hydrator;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;
use Ramsey\Uuid\Uuid;

class AccountCrudAction extends CrudAction
{
    protected $viewPath = "@account/admin";

    protected $routePrefix = "account.admin";

    /**
     * @var ConfirmationMailer
     */
    private $mailer;

    public function __construct(
        RendererInterface $renderer,
        Router $router,
        UserTable $table,
        FlashService $flash,
        ConfirmationMailer $mailer
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->mailer = $mailer;
    }

    protected function getNewEntity()
    {
        return new User();
    }

    public function create(ServerRequestInterface $request)
    {
        $user = $this->getNewEntity();

        if ($request->getMethod() === 'POST') {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $this->table->insert($this->getParams($request, $user));

                // TODO: Récupérer sans refaire une requête
                $user = $this->table->findBy('email', $request->getParsedBody()['email']);
                $token = $this->table->setConfirmation($user->getId());
                $this->mailer->send($user->getEmail(), [
                    'id' => $user->getId(),
                    'token' => $token
                ]);

                $this->flash->success('Un email de confirmation vous a été envoyé.');
                return $this->redirect($this->routePrefix . '.index');
            }
            Hydrator::hydrate($request->getParsedBody(), $this->table);
            $errors = $validator->getErrors();
        }

        return $this->renderer->render($this->viewPath . '/create', compact('item', 'errors'));
    }

    protected function getParams(ServerRequestInterface $request, $user) : array
    {
        $params = $request->getParsedBody();

        // mot de passe aléatoire par sécurité
        $params['password'] = password_hash(Uuid::uuid4(), PASSWORD_DEFAULT);

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'firstname',
                'lastname',
                'email',
                'password'
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    protected function getValidator(ServerRequestInterface $request): Validator
    {
        $validator = parent::getValidator($request)
            ->required('firstname', 'lastname', 'email')
            ->email('email')
            ->notEmpty('firstname', 'lastname')
            ->unique('email', $this->table, $this->table->getPdo(), $request->getAttribute('id'));

        return $validator;
    }
}
