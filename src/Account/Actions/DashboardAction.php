<?php

namespace App\Account\Actions;

use App\Account\AccountWidgetInterface;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

class DashboardAction
{
    private $renderer;

    /**
     * @var WidgetInterface[]
     */
    private $widgets;

    public function __construct(RendererInterface $renderer, array $widgets)
    {
        $this->renderer = $renderer;
        $this->widgets = $widgets;
    }

    public function __invoke()
    {
        $widgets = array_reduce($this->widgets, function (string $html, WidgetInterface $widget) {
            return $html . $widget->render();
        }, '');
        return $this->renderer->render('@account/dashboard', compact('widgets'));
    }
}
