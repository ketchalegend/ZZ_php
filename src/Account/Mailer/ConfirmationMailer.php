<?php

namespace App\Account\Mailer;

use Framework\Renderer\RendererInterface;

class ConfirmationMailer
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var string
     */
    private $from;

    public function __construct(\Swift_Mailer $mailer, RendererInterface $renderer, string $from)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
        $this->from = $from;
    }

    public function send(string $to, $params)
    {
        $message = new \Swift_Message(
            'Confirmation de votre compte',
            $this->renderer->render('@account/email/confirm.text', $params)
        );
        $message->addPart($this->renderer->render('@account/email/confirm.html', $params), 'text/html');
        $message->setTo($to);
        $message->setFrom($this->from);

        $this->mailer->send($message);
    }
}
