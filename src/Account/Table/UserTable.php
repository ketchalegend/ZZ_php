<?php

namespace App\Account\Table;

use App\Account\User;
use Framework\Database\Table;
use Ramsey\Uuid\Uuid;

class UserTable extends Table
{
    protected $entity = User::class;

    protected $table = 'users';

    public function setConfirmation(int $id) : string
    {
        $token = Uuid::uuid4()->toString();

        $this->update($id, [
            'confirmed_token' => $token
        ]);

        return $token;
    }

    public function confirm($id) : void
    {
        $this->update($id, [
            'confirmed_token' => null,
            'confirmed_at' => date('Y-m-d H:i:s')
        ]);
    }
}
