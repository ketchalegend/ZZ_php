<?php

namespace App\Account;

use App\Account\Actions\DashboardAction;
use App\Account\Mailer\ConfirmationMailer;
use function DI\autowire;
use function DI\get;
use function DI\add;

return [
    'account.prefix' => '/espaceZZ',
    'auth.entity' => User::class,
    'account.widgets' => [
        get(AccountWidget::class)
    ],
    'admin.widgets' => add([
        get(AccountAdminWidget::class)
    ]),
    DashboardAction::class => autowire()->constructorParameter('widgets', get('account.widgets')),
    ConfirmationMailer::class => autowire()->constructorParameter('from', get('mail.from'))
];
