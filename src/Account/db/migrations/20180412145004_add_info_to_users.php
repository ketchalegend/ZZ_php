<?php


use Phinx\Migration\AbstractMigration;

class AddInfoToUsers extends AbstractMigration
{
    public function change()
    {
        $this->table('users')
            ->addColumn('nickname', 'string', ['null' => true])
            ->addIndex(['nickname'], ['unique' => true])
            ->update();
    }
}
