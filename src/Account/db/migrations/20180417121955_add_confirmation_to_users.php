<?php


use Phinx\Migration\AbstractMigration;

class AddConfirmationToUsers extends AbstractMigration
{

    public function change()
    {
        $this->table('users')
            ->addColumn('confirmed_token', 'string', ['null' => true])
            ->addColumn('confirmed_at', 'datetime', ['null' => true])
            ->update();
    }
}
