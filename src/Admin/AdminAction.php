<?php

namespace App\Admin;

use App\Auth\Table\RoleTable;
use App\Auth\Table\UserToRoleTable;
use App\Auth\UserTable;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class AdminAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var UserTable
     */
    private $userTable;
    /**
     * @var RoleTable
     */
    private $roleTable;
    /**
     * @var UserToRoleTable
     */
    private $userToRoleTable;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var Router
     */
    private $router;

    public function __construct(
        RendererInterface $renderer,
        UserTable $userTable,
        RoleTable $roleTable,
        UserToRoleTable $userToRoleTable,
        FlashService $flashService,
        Router $router
    ) {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
        $this->roleTable = $roleTable;
        $this->userToRoleTable = $userToRoleTable;
        $this->flashService = $flashService;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        if ($request->getMethod() === 'DELETE') {
            return $this->delete($request);
        }

        if ($request->getMethod() === 'POST') {
            return $this->addMember($request);
        }

        return $this->index();
    }

    private function index()
    {
        $members = $this->userTable->findAdmins() ;

        return $this->renderer->render('@admin/member', compact('members'));
    }

    private function addMember(ServerRequestInterface $request)
    {
        $validator = $this->getValidator($request);

        if ($validator->isValid()) {
            $params = $request->getParsedBody();
            $user_id = $this->userTable->findBy('email', $params['email'])->id;

            $prefix = "/admin";
            $role_id = $this->roleTable->findByPrefix($prefix)->id;

            $this->userToRoleTable->insert([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
            $this->flashService->success("Le role à bien été ajouté");
            return new RedirectResponse($this->router->generateUri('admin.member'));
        }
        $errors = $validator->getErrors();

        $members = $this->userTable->findAdmins();

        return $this->renderer->render('@admin/member', compact('errors', 'members'));
    }

    private function delete(ServerRequestInterface $request)
    {
        $user_id = $request->getAttribute('id');

        $role = $this->roleTable->findAdminPermission($user_id);

        $this->userToRoleTable->delete($role->id);
        $this->flashService->success('Le role à bien été supprimé');

        return new RedirectResponse($this->router->generateUri('admin.member'));
    }

    protected function getParams(ServerRequestInterface $request) : array
    {
        $params = $request->getParsedBody();

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'email',
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Validator
     */
    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        return (new Validator($request->getParsedBody()))
            ->required('email')
            ->notEmpty('email')
            ->email('email')
            ->exists('email', 'email', 'users', $this->userTable->getPdo());
    }
}
