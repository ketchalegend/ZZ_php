<?php

use App\Admin\AdminMemberWidget;
use App\Admin\AdminModule;
use App\Admin\DashboardAction;

return [
    'admin.prefix' => '/admin',
    'admin.widgets' => [
        \DI\get(AdminMemberWidget::class)
    ],
    AdminModule::class => \DI\autowire()->constructorParameter('prefix', \DI\get('admin.prefix')),
    DashboardAction::class => \DI\autowire()->constructorParameter('widgets', \DI\get('admin.widgets'))
];
