<?php

namespace App\Auth;

use App\Auth\Actions\LoginAction;
use App\Auth\Actions\LoginAttemptAction;
use App\Auth\Actions\LogoutAction;
use App\Auth\Actions\PasswordForgetAction;
use App\Auth\Actions\PasswordResetAction;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Container\ContainerInterface;

class AuthModule extends Module
{
    const DEFINITIONS = __DIR__ . '/config.php';

    const MIGRATIONS = __DIR__ . '/db/migrations';

    const SEEDS = __DIR__ . '/db/seeds';

    public function __construct(ContainerInterface $container, Router $router, RendererInterface $renderer)
    {
        $renderer->addPath('auth', __DIR__ . '/views');
        $router->get($container->get('auth.login'), LoginAction::class, 'auth.login');
        $router->post($container->get('auth.login'), LoginAttemptAction::class);
        $router->post('/logout', LogoutAction::class, 'auth.logout');

        $router->get('/password', PasswordForgetAction::class, 'auth.password');
        $router->post('/password', PasswordForgetAction::class);

        $router->get(
            '/password/reset/{id}/{token}',
            PasswordResetAction::class,
            'auth.reset',
            [
                'id' => '\d+'
            ]
        );
        $router->post(
            '/password/reset/{id}/{token}',
            PasswordResetAction::class,
            null,
            ['id' => '\d+']
        );
    }
}
