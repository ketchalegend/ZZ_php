<?php

namespace App\Auth;

use Framework\Auth;
use Framework\Auth\User;
use Framework\Database\NoRecordException;
use Framework\Session\SessionInterface;

class DatabaseAuth implements Auth
{

    /**
     * @var UserTable
     */
    private $userTable;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var User
     */
    private $user;

    public function __construct(UserTable $userTable, SessionInterface $session)
    {
        $this->userTable = $userTable;
        $this->session = $session;
    }

    /**
     * @param string $email
     * @param string $password
     * @return User
     * @throws NotConfirmedAccountException
     */
    public function login(string $email, string $password) : ?User
    {
        if (empty($email) || empty($password)) {
            return null;
        }

        try {
            $user = $this->userTable->findBy('email', $email);
        } catch (NoRecordException $e) {
            return null;
        }

        if ($user->getConfirmedToken() !== null &&
            $user->getConfirmedAt() === null
        ) {
            throw new NotConfirmedAccountException();
        }

        if ($user && password_verify($password, $user->password)) {
            $this->setUser($user);
            return $user;
        }

        return null;
    }

    public function logout() : void
    {
        $this->session->delete('auth.user');
    }

    public function getUser(): ?User
    {
        if ($this->user) {
            return $this->user;
        }

        $userId = $this->session->get('auth.user');
        if ($userId) {
            try {
                $this->user = $this->userTable->find($userId);
                return $this->user;
            } catch (NoRecordException $e) {
                $this->session->delete('auth.user');
                return null;
            }
        }
        return null;
    }

    public function setUser(\App\Auth\User $user) : void
    {
        $this->session->set('auth.user', $user->id);
        $this->user = $user;
    }
}
