<?php

namespace App\Auth;

use Framework\Auth\ForbiddenException;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ForbiddenMiddleware implements MiddlewareInterface
{

    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var string
     */
    private $redirectPath;

    public function __construct(string $redirectPath, FlashService $flashService)
    {
        $this->flashService = $flashService;
        $this->redirectPath = $redirectPath;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (ForbiddenException $exception) {
            $msg = "Vous avez pas le droit d'accéder à cette page, vous devez posséder un compteavec plus de droits.";
            $this->flashService->error($msg);
            return new RedirectResponse($this->redirectPath);
        }
    }
}
