<?php

namespace App\Auth;

use App\Auth\Table\RoleTable;
use Framework\Auth;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RolesMiddleware implements MiddlewareInterface
{

    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var RoleTable
     */
    private $roleTable;

    public function __construct(
        Auth $auth,
        RoleTable $roleTable,
        FlashService $flashService
    ) {
        $this->auth = $auth;
        $this->flashService = $flashService;
        $this->roleTable = $roleTable;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // TOUS LES PREFIX SECURISES
        $regulatedPrefixes = $this->roleTable->findAllPrefixes();
        $regulatedPrefixesArray = [];
        foreach ($regulatedPrefixes as $prefix) {
            $regulatedPrefixesArray[] = $prefix->prefix;
        }

        // ON TROUVE LA ROUTE ACTUELLE
        $requestedPrefix = '/' . explode('/', $request->getUri()->getPath())[1];

        /** @var \App\Account\User $user */
        $user = $this->auth->getUser();

        if (in_array($requestedPrefix, $regulatedPrefixesArray)) {
            if ($user !== null) {
                // CHERCHER SES PREFIX
                $prefixes = $this->roleTable->findAllowedPrefixes($user->id);
                $prefixesArray = [];
                foreach ($prefixes as $prefix) {
                    $prefixesArray[] = $prefix->prefix;
                }
                if (in_array($requestedPrefix, $prefixesArray)) {
                    return $handler->handle($request);
                }
            }

            $msg ="Vous n'avez pas le droit d'accéder à cette page, vous devez posséder un compte avec plus de droits.";
            $this->flashService->error($msg);
            return new RedirectResponse('/login');
        }

        return $handler->handle($request);
    }
}
