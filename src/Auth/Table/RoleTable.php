<?php

namespace App\Auth\Table;

use App\Auth\Entity\Role;
use Framework\Database\Table;

class RoleTable extends Table
{
    protected $entity = Role::class;

    protected $table = 'roles';

    public function findAllowedPrefixes(int $user_id)
    {
        return $this->makeQuery()
            ->select('prefix')
            ->from('roles', 'r')
            ->join('user2role', 'r.id = user2role.role_id')
            ->where("user_id = $user_id")
            ->fetchAll();
    }

    public function findAllPrefixes()
    {
        return $this->findAll()->select('prefix')->fetchAll();
    }

    public function findByPrefix(string $prefix)
    {
        return $this->makeQuery()
            ->select('id')
            ->where("prefix = '$prefix'")
            ->fetch();
    }

    public function findUserPrefixes(int $user_id)
    {
        return $this->makeQuery()
            ->select('prefix')
            ->from('roles', 'r')
            ->join('user2role', 'r.id = user2role.role_id')
            ->where("user_id = $user_id")
            ->fetchAll();
    }

    public function findAdminPermission(int $user_id)
    {
        return $this->makeQuery()
            ->select('u2r.id')
            ->from('user2role u2r')
            ->join('roles', 'u2r.role_id = roles.id')
            ->where("user_id = $user_id", "roles.prefix = '/admin'")
            ->fetchOrFail();
    }

    public function findClubPermission(int $user_id, string $slug)
    {
        return $this->makeQuery()
            ->select('u2r.id')
            ->from('user2role u2r')
            ->join('roles', 'u2r.role_id = roles.id')
            ->where("user_id = $user_id", "roles.prefix = '/$slug-admin'")
            ->fetchOrFail();
    }
}
