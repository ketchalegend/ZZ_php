<?php

namespace App\Auth;

class User implements \Framework\Auth\User
{
    public $id;

    public $firstname;

    public $lastname;

    public $email;

    public $password;

    public $passwordReset;

    public $passwordResetAt;

    public function getUsername(): string
    {
        return $this->firstname;
    }

    public function getRoles(): array
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getPasswordReset()
    {
        return $this->passwordReset;
    }

    /**
     * @param mixed $passwordReset
     */
    public function setPasswordReset($passwordReset): void
    {
        $this->passwordReset = $passwordReset;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetAt() : ?\DateTime
    {
        return $this->passwordResetAt;
    }

    /**
     * @param $date
     */
    public function setPasswordResetAt($date)
    {
        if (is_string($date)) {
            $this->passwordResetAt = new \DateTime();
        } else {
            $this->passwordResetAt = $date;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }
}
