<?php


use Phinx\Seed\AbstractSeed;

class RolesSeeder extends AbstractSeed
{
    public function run()
    {
        $roles = $this->table('roles');
        $user2role = $this->table('user2role');

        $rolesData = [];

        $user2role->dropForeignKey('role_id');
        $user2role->dropForeignKey('user_id');
        $roles->truncate();
        $user2role->truncate();

        $roles->insert(['prefix' => '/admin'])->save();

        $clubNames = ['isibouffe', 'pixel', 'rezzo', 'isibot'];

        foreach ($clubNames as $club) {
            $rolesData[] = [
                'prefix' => '/' . strtolower($club) . '-admin'
            ];
        }

        $roles->insert($rolesData)->save();

        $user2role->insert(['user_id' => '1', 'role_id' => '1'])->save();
        $user2role->insert(['user_id' => '1', 'role_id' => '2'])->save();
        $user2role->insert(['user_id' => '2', 'role_id' => '2'])->save();
        $user2role->insert(['user_id' => '2', 'role_id' => '3'])->save();

        $user2role->addForeignKey('role_id', 'roles', 'id', ['delete' => 'CASCADE']);
        $user2role->addForeignKey('user_id', 'users', 'id', ['delete' => 'CASCADE']);
    }
}
