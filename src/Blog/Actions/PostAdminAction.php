<?php

namespace App\Blog\Actions;

use App\Blog\Table\PostTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;

class PostAdminAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var PostTable
     */
    private $postTable;
    /**
     * @var FlashService
     */
    private $flashService;

    private $messages = [
        'activate' => "Le post '%s' à bien été %s."
    ];

    use RouterAwareAction;

    public function __construct(
        RendererInterface $renderer,
        Router $router,
        PostTable $postTable,
        FlashService $flashService
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->postTable = $postTable;
        $this->flashService = $flashService;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        if ($request->getAttribute('id')) {
            return $this->activate($request);
        }
    }

    private function activate(ServerRequestInterface $request)
    {
        $postId = $request->getAttribute('id');
        $post = $this->postTable->makeQuery()
            ->where("id = $postId")
            ->fetchOrFail();

        $this->postTable->update($postId, ['is_validated' => $post->isValidated ? 0 : 1]);

        $this->flashService->success(
            sprintf($this->messages['activate'], $post->name, $post->isValidated ? 'déactivé' : 'activé')
        );

        return $this->redirect('blog.admin.index');
    }
}
