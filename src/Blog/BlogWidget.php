<?php

namespace App\Blog;

use App\Blog\Table\PostTable;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

class BlogWidget implements WidgetInterface
{

    private $renderer;
    /**
     * @var PostTable
     */
    private $postTable;

    public function __construct(RendererInterface $renderer, PostTable $postTable)
    {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
    }

    public function render(): string
    {
        $posts = $this->postTable->getPostsNumber();
        $unvalidated = $this->postTable->getUnvalidatedPostsNumber();

        return $this->renderer->render('@blog/admin/posts/widget', compact('posts', 'unvalidated'));
    }
}
