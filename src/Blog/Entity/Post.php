<?php

namespace App\Blog\Entity;

class Post
{
    public $id;
    
    public $name;

    public $slug;

    public $content;

    public $createdAt;

    public $updatedAt;

    public $createdBy;

    public $isValidated;

    public function setCreatedAt($datetime)
    {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

    public function setUpdatedAt($datetime)
    {
        if (is_string($datetime)) {
            $this->updatedAt = new \DateTime($datetime);
        } else {
            $this->updatedAt = $datetime;
        }
    }
}
