<?php

namespace App\Blog\Table;

use App\Blog\Entity\Post;
use Framework\Database\PaginatedQuery;
use Framework\Database\Query;
use Framework\Database\Table;
use Pagerfanta\Pagerfanta;

class PostTable extends Table
{
    protected $entity = Post::class;

    protected $table = 'posts';

    public function findAll() : Query
    {
        return $this->makeQuery()
            ->order('created_at DESC');
    }

    public function findById(int $id) : Post
    {
        return $this->findAll()
            ->where("id = $id")
            ->fetch();
    }

    public function findAllValidated() : Query
    {
        return $this->findAll()
            ->where("is_validated = 1", 'created_at <= CURDATE()');
    }

    public function findAllOrderByActivation()
    {
        return $this->makeQuery()
            ->order("is_validated ASC, created_at DESC");
    }

    public function getPostsNumber()
    {
        return $this->makeQuery()->count();
    }

    public function getUnvalidatedPostsNumber()
    {
        return $this->makeQuery()->where('is_validated = 0')->count();
    }

    public function getClubPostsNumber($club_id)
    {
        return $this->makeQuery()->where("created_by = '$club_id'")->count();
    }

    public function getClubUnvalidatedPostsNumber($club_id)
    {
        return $this->makeQuery()->where("created_by = '$club_id'", "is_validated = 0")->count();
    }
}
