<?php

use Phinx\Seed\AbstractSeed;

class PostSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create('fr_FR');
        $posts = $this->table('posts');

        // Vide la table pour éviter les duplications
        $posts->truncate();


        for ($i = 0; $i < 100; $i++) {
            $date = $faker->unixTime('now');
            $data[] = [
                'name' => $faker->catchPhrase,
                'slug' => $faker->domainWord,
                'content' => $faker->text(3000),
                'created_at' => date('Y-m-d H:i:s', $date),
                'updated_at' => date('Y-m-d H:i:s', $date),
                'created_by' => rand(1, 4),
                'is_validated' => rand(1, 10) == 1 ? 0 : 1
            ];
        }

        $posts->insert($data)->save();
    }
}
