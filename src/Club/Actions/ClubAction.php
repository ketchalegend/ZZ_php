<?php

namespace App\Club\Actions;

use App\Auth\UserTable;
use App\Club\Table\ClubTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClubAction
{

    private $renderer;

    private $router;

    private $clubTable;
    /**
     * @var UserTable
     */
    private $userTable;

    use RouterAwareAction;

    public function __construct(
        RendererInterface $renderer,
        Router $router,
        ClubTable $clubTable,
        UserTable $userTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->clubTable = $clubTable;
        $this->userTable = $userTable;
    }

    /**
     * @param Request $request
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Framework\Database\NoRecordException
     */
    public function __invoke(Request $request)
    {
        if ($request->getAttribute('id')) {
            return $this->show($request);
        } else {
            return $this->index();
        }
    }

    public function index() : string
    {
        $clubs = $this->clubTable->findActive();

        return $this->renderer->render('@club/index', compact('clubs'));
    }

    /**
     * @param Request $request
     * @return \Psr\Http\Message\ResponseInterface|string
     * @throws \Framework\Database\NoRecordException
     */
    public function show(Request $request)
    {
        $slug = $request->getAttribute('slug');
        $club = $this->clubTable->find($request->getAttribute('id'));
        // Si le slug n'est pas celui attendu, on redirige vers la page avec le bon slug
        if ($club->slug != $slug) {
            return $this->redirect('club.show', [
                'slug' => $club->slug,
                'id' => $club->id
            ]);
        }

        $membres = $this->userTable->findClubMembers($slug);

        return $this->renderer->render('@club/show', compact('club', 'membres'));
    }
}
