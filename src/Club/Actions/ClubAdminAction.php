<?php

namespace App\Club\Actions;

use App\Auth\Table\RoleTable;
use App\Auth\Table\UserToRoleTable;
use App\Auth\UserTable;
use App\Club\ClubUpload;
use App\Club\Table\ClubTable;
use Framework\Actions\RouterAwareAction;
use Framework\Auth;
use Framework\Database\Hydrator;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Router\Route;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class ClubAdminAction
{

    /**
     * @var ClubTable
     */
    private $clubTable;
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var ClubUpload
     */
    private $clubUpload;
    /**
     * @var RoleTable
     */
    private $roleTable;
    /**
     * @var UserTable
     */
    private $userTable;
    /**
     * @var UserToRoleTable
     */
    private $userToRoleTable;

    private $messages = [
        'create' => "Le club '%s' à bien été ajouté.",
        'delete' => "Le club '%s' à bien été supprimé.",
        'activate' => "Le club '%s' à bien été %s."
    ];

    use RouterAwareAction;

    public function __construct(
        RendererInterface $renderer,
        ClubTable $clubTable,
        FlashService $flashService,
        Router $router,
        ClubUpload $clubUpload,
        RoleTable $roleTable,
        UserTable $userTable,
        UserToRoleTable $userToRoleTable
    ) {
        $this->clubTable = $clubTable;
        $this->renderer = $renderer;
        $this->flashService = $flashService;
        $this->router = $router;
        $this->clubUpload = $clubUpload;
        $this->roleTable = $roleTable;
        $this->userTable = $userTable;
        $this->userToRoleTable = $userToRoleTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        if ($request->getMethod() === 'DELETE') {
            return $this->delete($request);
        }

        if ($request->getAttribute('id')) {
            return $this->activate($request);
        }

        if (substr((string) $request->getUri(), -5) === 'creer') {
            return $this->create($request);
        }
        return $this->index($request);
    }

    private function index(ServerRequestInterface $request)
    {
        $params = $request->getQueryParams();
        $clubs = $this->clubTable->findAll()->order('name ASC')->paginate(12, $params['p'] ?? 1);

        return $this->renderer->render('@club/admin/clubs/index', compact('clubs'));
    }

    private function create(ServerRequestInterface $request)
    {
        if ($request->getMethod() === 'POST') {
            $validator = $this->getValidator($request);

            if ($validator->isValid()) {
                $params = $this->getParams($request);
                $this->clubTable->insert($params);

                // Ajout du role
                $prefix = '/' . $request->getParsedBody()['slug'] . '-admin';
                $this->roleTable->insert(compact('prefix'));
                $role_id = $this->roleTable->findByPrefix($prefix)->id;

                // Ajouter L'association user -> role
                $presidentEmail = $request->getParsedBody()['president_email'];

                $user_id = $this->userTable->findBy('email', $presidentEmail)->id;
                $this->userToRoleTable->insert([
                    'user_id' => $user_id,
                    'role_id' => $role_id
                ]);
                $this->flashService->success(sprintf($this->messages['create'], $params['name']));
                return $this->redirect('club.admin.index');
            }
            $errors = $validator->getErrors();
        }

        return $this->renderer->render('@club/admin/clubs/create', compact('errors'));
    }

    private function activate(ServerRequestInterface $request)
    {
        $clubId = $request->getAttribute('id');
        $club = $this->clubTable->find($clubId);
        if ($club->isActive == 1) {
            $role = $this->roleTable->findByPrefix("/{$club->slug}-admin");

            $statement = $this->userToRoleTable->getPdo()
                ->prepare("DELETE FROM {$this->userToRoleTable->getTable()} WHERE role_id = ?");
            $statement->execute([$role->id]);
        }
        $this->clubTable->update($clubId, ['is_active' => $club->isActive ? 0 : 1]);

        $this->flashService->success(
            sprintf($this->messages['activate'], $club->name, $club->isActive ? 'déactivé' : 'activé')
        );

        return $this->redirect('club.admin.index');
    }

    private function delete(ServerRequestInterface $request)
    {
        $club = $this->clubTable->find($request->getAttribute('id'));
        $this->clubUpload->delete($club->logo);

        $role = $this->roleTable->findByPrefix('/' . $club->slug . '-admin');
        if ($role) {
            $this->roleTable->delete($role->id);
        }

        $this->clubTable->delete($request->getAttribute('id'));
        $this->flashService->success(sprintf($this->messages['delete'], $club->name));
        return new RedirectResponse($this->router->generateUri('club.admin.index'));
    }

    /**
     * Filtre des paramètres particuliers
     * @param ServerRequestInterface $request
     * @return array
     */
    protected function getParams(ServerRequestInterface $request) : array
    {
        $params = $request->getParsedBody();

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'name',
                'slug'
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        return (new Validator($request->getParsedBody()))
            ->required('slug', 'name', 'president_email')
            ->notEmpty('president_email')
            ->exists('email', 'president_email', 'users', $this->userTable->getPdo())
            ->slug('slug')
            ->length('name', 3)
            ->unique('slug', $this->clubTable, $this->clubTable->getPdo(), true)
            ->unique('name', $this->clubTable, $this->clubTable->getPdo(), true);
    }
}
