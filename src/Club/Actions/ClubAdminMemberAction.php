<?php

namespace App\Club\Actions;

use App\Auth\Table\RoleTable;
use App\Auth\Table\UserToRoleTable;
use App\Auth\UserTable;
use App\Club\Table\ClubTable;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class ClubAdminMemberAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var ClubTable
     */
    private $clubTable;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var UserTable
     */
    private $userTable;

    private $mesages = [
        'delete' => "Le role à bien été enlevé"
    ];
    /**
     * @var Router
     */
    private $router;
    /**
     * @var RoleTable
     */
    private $roleTable;
    /**
     * @var UserToRoleTable
     */
    private $userToRoleTable;

    public function __construct(
        RendererInterface $renderer,
        ClubTable $clubTable,
        FlashService $flashService,
        UserTable $userTable,
        RoleTable $roleTable,
        UserToRoleTable $userToRoleTable,
        Router $router
    ) {

        $this->renderer = $renderer;
        $this->clubTable = $clubTable;
        $this->flashService = $flashService;
        $this->userTable = $userTable;
        $this->router = $router;
        $this->roleTable = $roleTable;
        $this->userToRoleTable = $userToRoleTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        if ($request->getMethod() === 'POST') {
            return $this->addMember($request);
        }

        if ($request->getMethod() === 'DELETE') {
            return $this->delete($request);
        }

        $slug = $request->getAttribute('slug');
        try {
            $this->clubTable->findBy('slug', $slug);
        } catch (NoRecordException $e) {
            $this->flashService->error("Ce club n'exite pas.");
            return new RedirectResponse($this->router->generateUri('club.admin.dashboard', compact('slug')));
        }
        return $this->index($request);
    }

    private function index(ServerRequestInterface $request)
    {
        $slug = $request->getAttribute('slug');
        $members = $this->userTable->findClubMembers($slug);

        return $this->renderer->render('@club/admin/clubs/member', compact('members', 'slug'));
    }

    private function delete(ServerRequestInterface $request)
    {
        $user_id = $request->getAttribute('id');
        $slug = $request->getAttribute('slug');


        $role = $this->roleTable->findClubPermission($user_id, $slug);

        $this->userToRoleTable->delete($role->id);
        $this->flashService->success($this->mesages['delete']);

        return new RedirectResponse($this->router->generateUri('club.admin.member', compact('slug')));
    }

    private function addMember(ServerRequestInterface $request)
    {
        $validator = $this->getValidator($request);
        $slug = $request->getAttribute('slug');

        if ($validator->isValid()) {
            $params = $request->getParsedBody();
            $user_id = $this->userTable->findBy('email', $params['email'])->id;

            $prefix = "/$slug-admin";
            $role_id = $this->roleTable->findByPrefix($prefix)->id;

            $this->userToRoleTable->insert([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
            $this->flashService->success("Le role à bien été ajouté");
            return new RedirectResponse($this->router->generateUri('club.admin.member', compact('slug')));
        }
        $errors = $validator->getErrors();

        $members = $this->userTable->findClubMembers($slug);

        return $this->renderer->render('@club/admin/clubs/member', compact('errors', 'slug', 'members'));
    }

    protected function getParams(ServerRequestInterface $request) : array
    {
        $params = $request->getParsedBody();

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'email',
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Validator
     */
    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        return (new Validator($request->getParsedBody()))
            ->required('email')
            ->notEmpty('email')
            ->email('email')
            ->exists('email', 'email', 'users', $this->userTable->getPdo());
    }
}
