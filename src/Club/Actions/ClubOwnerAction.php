<?php

namespace App\Club\Actions;

use App\Auth\Table\RoleTable;
use App\Club\ClubUpload;
use App\Club\Entity\Club;
use App\Club\Table\ClubTable;
use Framework\Actions\RouterAwareAction;
use Framework\Database\Hydrator;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class ClubOwnerAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var ClubTable
     */
    private $clubTable;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var ClubUpload
     */
    private $clubUpload;
    /**
     * @var RoleTable
     */
    private $roleTable;

    private $messages = [
        'edit' => "Le club '%s' à bien été modifié."
    ];

    public function __construct(
        RendererInterface $renderer,
        ClubTable $clubTable,
        Router $router,
        FlashService $flashService,
        ClubUpload $clubUpload,
        RoleTable $roleTable
    ) {
        $this->renderer = $renderer;
        $this->clubTable = $clubTable;
        $this->router = $router;
        $this->flashService = $flashService;
        $this->clubUpload = $clubUpload;
        $this->roleTable = $roleTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        if ($request->getMethod() === 'POST') {
            return $this->edit($request);
        }

        $slug = $request->getAttribute('slug');
        try {
            $club = $this->clubTable->findBy('slug', $slug);
        } catch (NoRecordException $e) {
            $this->flashService->error("Ce club n'existe pas");
            return new RedirectResponse($this->router->generateUri('account.dashboard'));
        }
        return $this->renderer->render('@club/admin/owner/edit', compact('club'));
    }

    private function edit(ServerRequestInterface $request)
    {
        /** @var Club $club */
        $slug = $request->getAttribute('slug');
        $club = $this->clubTable->findBy('slug', $slug);

        $validator = $this->getValidator($request);
        if ($validator->isValid()) {
            $this->clubTable->update($club->id, $this->getParams($request, $club));
            $newSlug = $request->getParsedBody()['slug'];
            if ($slug != $newSlug) {
                $role_id = $this->roleTable->findBy('prefix', "/$slug-admin");
                $this->roleTable->update($role_id->id, ['prefix' => "/$newSlug-admin"]);
            }
            $this->flashService->success(sprintf($this->messages['edit'], $club->name));
            return new RedirectResponse($this->router->generateUri('club.owner.edit', ['slug' => $newSlug]));
        }
        $errors = $validator->getErrors();
        Hydrator::hydrate($request->getParsedBody(), $club);

        return $this->renderer->render('@club/admin/owner/edit', compact('club', 'errors'));
    }

    protected function getParams(ServerRequestInterface $request, $club) : array
    {
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
        $logo = $this->clubUpload->upload($params['logo'], $club->logo);
        if ($logo) {
            $params['logo'] = $logo;
        } else {
            unset($params['logo']);
        }

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'short_description',
                'description',
                'name',
                'slug',
                'created_at',
                'logo'
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Validator
     * @throws NoRecordException
     */
    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        $clubId = $this->clubTable->findBy('slug', $request->getAttribute('slug'))->id;
        return (new Validator($request->getParsedBody()))
            ->required('short_description', 'description', 'name', 'slug', 'created_at')
            ->length('short_description', 4, 200)
            ->length('description', 10, 3000)
            ->length('slug', 2, 50)
            ->unique('slug', 'clubs', $this->clubTable->getPdo(), $clubId)
            ->length('name', 3, 32)
            ->dateTime('created_at')
            ->extension('logo', ['jpg', 'png'])
            ->maxSize('logo', 32000000)
            ->slug('slug');
    }
}
