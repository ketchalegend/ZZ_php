<?php

namespace App\Club;

use App\Auth\Table\RoleTable;
use App\Club\Table\ClubTable;
use Framework\Auth;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

/**
 * Permet d'aller vers les clubs autorisés
 * Class ClubOwnerWidget
 * @package App\Club
 */
class ClubOwnerWidget implements WidgetInterface
{

    private $renderer;
    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var RoleTable
     */
    private $roleTable;
    /**
     * @var ClubTable
     */
    private $clubTable;

    public function __construct(
        RendererInterface $renderer,
        Auth $auth,
        RoleTable $roleTable,
        ClubTable $clubTable
    ) {
        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->roleTable = $roleTable;
        $this->clubTable = $clubTable;
    }

    public function render(): string
    {
        $user_id = $this->auth->getUser()->id;
        $roles = $this->roleTable->findUserPrefixes($user_id);

        $slugs = [];
        foreach ($roles as $role) {
            $slugs[] = substr(explode('-', $role->prefix)[0], 1);
        }

        $admin = array_filter($roles->getRecords(), function ($key) {
            return $key['prefix'] == '/admin';
        });

        // récupère les clubs
        $clubs = $this->clubTable->findBySlugs($slugs);

        if ($clubs->hasNoRecords()) {
            $clubs = false;
        }

        return $this->renderer->render('@club/admin/owner/widget', compact('clubs', 'admin'));
    }
}
