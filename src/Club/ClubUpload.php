<?php

namespace App\Club;

use Framework\Upload;

class ClubUpload extends Upload
{

    protected $path = 'public/uploads/clubs';

    protected $formats = [
        'thumb' => [400, 400]
    ];
}
