<?php

namespace App\Club\Table;

use App\Club\Entity\Club;
use Framework\Database\Table;

class ClubTable extends Table
{
    protected $entity = Club::class;

    protected $table = 'clubs';

    public function findActive()
    {
        return $this->makeQuery()->where("is_active = true");
    }

    public function findBySlugs(array $slugs)
    {
        return $this->makeQuery()
            ->where("slug IN ('" . join("','", $slugs) . "')")
            ->order('name ASC')
            ->fetchAll();
    }

    public function getActiveClubsNumber()
    {
        return $this->makeQuery()->where('is_active = 1')->count();
    }

    public function getInactiveClubsNumber()
    {
        return $this->makeQuery()->where('is_active = 0')->count();
    }
}
