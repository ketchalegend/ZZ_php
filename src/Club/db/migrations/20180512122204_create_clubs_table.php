<?php


use Phinx\Migration\AbstractMigration;

class CreateClubsTable extends AbstractMigration
{

    public function change()
    {
        $this->table('clubs')
            ->addColumn('name', 'string')
            ->addColumn('slug', 'string')
            ->addColumn('description', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => true
            ])
            ->addColumn('short_description', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_SMALL,
                'null' => true
            ])
            ->addColumn('created_at', 'datetime', [
                'null' => true
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => false
            ])
            ->addColumn('logo', 'string', ['null' => true])
            ->create();
    }
}
