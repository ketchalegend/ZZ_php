<?php


use Phinx\Migration\AbstractMigration;

class AddClubsToPost extends AbstractMigration
{
    public function change()
    {
        $this->table('posts')
            ->addColumn('created_by', 'integer', ['null' => true])
            ->addForeignKey('created_by', 'clubs', 'id', ['delete' => 'SET_NULL'])
            ->addColumn('is_validated', 'boolean', ['default' => 0])
            ->update();
    }
}
