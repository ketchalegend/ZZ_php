<?php

namespace Framework\Database;

use Pagerfanta\Adapter\AdapterInterface;

class PaginatedQuery implements AdapterInterface
{
    private $query;

    /**
     * PaginatedQuery constructor.
     * @param Query $query
     */
    public function __construct(Query $query)
    {
        $this->query = $query;
    }

    /**
     * @return integer le nombre de résultats.
     */
    public function getNbResults() : int
    {
        return $this->query->count();
    }


    /**
     * Retourne un échantillon de résultats
     * @param int $offset
     * @param int $length
     * @return QueryResult the slice
     */
    public function getSlice($offset, $length) : QueryResult
    {
        $query = clone $this->query;
        return $query->limit($length, $offset)->fetchAll();
    }
}
