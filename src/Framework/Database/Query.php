<?php

namespace Framework\Database;

use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;

class Query implements \IteratorAggregate
{
    private $select;

    private $from;

    private $where = [];

    private $joins;

    private $order = [];

    private $limit;

    private $params = [];

    private $entity;

    private $pdo;

    public function __construct(\PDO $pdo = null)
    {
        $this->pdo = $pdo;
    }

    public function __toString()
    {
        $parts = ['SELECT'];
        if ($this->select) {
            $parts[] = join(', ', $this->select);
        } else {
            $parts[] = '*';
        }

        $parts[] = 'FROM';
        $parts[] = $this->buildFrom();

        if (!empty($this->joins)) {
            foreach ($this->joins as $type => $joins) {
                foreach ($joins as [$table, $condition]) {
                    $parts[] = strtoupper($type) . " JOIN $table ON $condition";
                }
            }
        }

        if (!empty($this->where)) {
            $parts[] = 'WHERE';
            $parts[] = '(' . join(') AND (', $this->where) . ')';
        }

        if (!empty($this->order)) {
            $parts[] = 'ORDER BY';
            $parts[] = join(', ', $this->order);
        }

        if ($this->limit) {
            $parts[] = 'LIMIT ' . $this->limit;
        }

        return join(' ', $parts);
    }

    public function from(string $table, ?string $alias = null) : self
    {
        if ($alias) {
            $this->from[$table] = $alias;
        } else {
            $this->from[] = $table;
        }
        return $this;
    }

    public function select(string... $fields) : self
    {
        $this->select = $fields;
        return $this;
    }

    public function limit(int $length, ?int $offset = 0) : self
    {
        $this->limit = "$offset, $length";
        return $this;
    }

    public function order(string $order) : self
    {
        $this->order[] = $order;
        return $this;
    }

    public function join(string $table, string $condition, string $type = 'left') : self
    {
        $this->joins[$type][] = [$table, $condition];
        return $this;
    }

    public function where(string... $condition) : self
    {
        $this->where = array_merge($this->where, $condition);
        return $this;
    }

    public function count() : int
    {
        $query = clone $this;
        $table = current($this->from);
        return $query->select("COUNT($table.id)")->execute()->fetchColumn();
    }

    public function params(array $params) : self
    {
        $this->params = array_merge($this->params, $params);
        return $this;
    }

    public function into(string $entity) : self
    {
        $this->entity = $entity;
        return $this;
    }

    public function fetchAll() : QueryResult
    {
        return new QueryResult(
            $this->execute()->fetchAll(\PDO::FETCH_ASSOC),
            $this->entity
        );
    }

    public function fetch()
    {
        $record = $this->execute()->fetch(\PDO::FETCH_ASSOC);
        if ($record === false) {
            return false;
        }
        if ($this->entity) {
            return Hydrator::hydrate($record, $this->entity);
        }
        return $record;
    }


    /**
     * retourne un résulat ou retourne un exception
     * @return bool|mixed
     * @throws NoRecordException
     */
    public function fetchOrFail()
    {
        $record = $this->fetch();
        if ($record === false) {
            throw new NoRecordException();
        }
        return $record;
    }

    /**
     * Pagine les résultats
     * @param int $perPage
     * @param int $currentPage
     * @return Pagerfanta
     */
    public function paginate(int $perPage, $currentPage = 1) : Pagerfanta
    {
        $paginator = new PaginatedQuery($this);
        $pagerFanta = (new Pagerfanta($paginator))->setMaxPerPage($perPage);

        try {
            $pagerFanta->setCurrentPage($currentPage);
        } catch (NotValidCurrentPageException $e) {
            $pagerFanta->setCurrentPage(1);
        }

        return $pagerFanta;
    }

    private function execute()
    {
        $query = $this->__toString();
        if (!empty($this->params)) {
            $statement = $this->pdo->prepare($query);
            $statement->execute($this->params);
            return $statement;
        }
        return $this->pdo->query($query);
    }

    private function buildFrom() : string
    {
        $from = [];
        foreach ($this->from as $key => $value) {
            if (is_string($key)) {
                $from[] = "$key as $value";
            } else {
                $from[] = $value;
            }
        }

        return join(', ', $from);
    }

    public function getIterator()
    {
        return $this->fetchAll();
    }
}
