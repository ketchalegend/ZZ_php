<?php

namespace Framework;

/**
 * Class Module
 * @package Framework
 */
class Module
{
    /**
     * Définitions du chemin vers les configurations des dépendances du module
     */
    const DEFINITIONS = null;

    /**
     * Définitions du chemin vers les migrations du module
     */
    const MIGRATIONS = null;

    /**
     * Définitions du chemin vers les seeds du module
     */
    const SEEDS = null;
}
