<?php

namespace Framework\Renderer;

/**
 * Interface RendererInterface
 * Donne une interface pour les renderers
 * @package Framework\Renderer
 */
interface RendererInterface
{
    /**
     * Ajoute le chemin de la vue
     * @param string $namespace
     * @param null|string $path
     */
    public function addPath(string $namespace, ?string $path = null) : void;

    /**
     * Rends une vue avec ces paramètres
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render(string $view, array $params = []) : string;

    /**
     * Ajoute des variables global pour les rendre disponible à la vue
     * @param string $key
     * @param $value
     */
    public function addGlobal(string $key, $value) : void;
}
