<?php

namespace Framework;

use Aura\Router\RouterContainer;
use Framework\Router\Route;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Router
 * @package Framework
 */
class Router
{
    private $routerContainer;

    private $map;

    /**
     * Constructeur du routeur
     * @param null|string $cache
     */
    public function __construct(?string $cache = null)
    {
        // TODO: Check pour le cache
        $this->routerContainer = new RouterContainer();
        $this->map = $this->routerContainer->getMap();
    }

    /**
     * @param string $path
     * @param $callable
     * @param null|string $name
     * @param array|null $tokens
     */
    public function get(string $path, $callable, ?string $name = null, ?array $tokens = [])
    {
        $this->map->get($name, $path, $callable)->tokens($tokens);
    }

    /**
     * @param string $path
     * @param $callable
     * @param null|string $name
     * @param array|null $tokens
     */
    public function post(string $path, $callable, ?string $name = null, ?array $tokens = [])
    {
        $this->map->post($name, $path, $callable)->tokens($tokens);
    }

    /**
     * @param string $path
     * @param $callable
     * @param null|string $name
     * @param array|null $tokens
     */
    public function delete(string $path, $callable, ?string $name = null, ?array $tokens = [])
    {
        $this->map->delete($name, $path, $callable)->tokens($tokens);
    }

    /**
     * Génère les route du CRUD
     * @param string $prefixPath
     * @param $callable
     * @param null|string $prefixName
     */
    public function crud(string $prefixPath, $callable, ?string $prefixName)
    {
        $this->get("$prefixPath", $callable, "$prefixName.index");

        $this->get("$prefixPath/new", $callable, "$prefixName.create");
        $this->post("$prefixPath/new", $callable);

        $this->get("$prefixPath/{id}", $callable, "$prefixName.edit", ["id" => "\d+"]);
        $this->post("$prefixPath/{id}", $callable, null, ["id" => "\d+"]);

        $this->delete("$prefixPath/{id}", $callable, "$prefixName.delete", ["id" => "\d+"]);
    }

    /**
     * Retourne une route qui correspond a celle demander dans la requete
     * @param ServerRequestInterface $request
     * @return Route|null
     */
    public function match(ServerRequestInterface $request): ?Route
    {
        $matcher = $this->routerContainer->getMatcher();
        $route = $matcher->match($request);
        if (!$route) {
            return null;
        }
        return new Route(
            $route->name,
            $route->handler,
            $route->attributes
        );
    }

    /**
     * Génère un URI basé sur la route
     * @param string $name
     * @param array $params
     * @param array $queryParams
     * @return null|string
     * @throws \Aura\Router\Exception\RouteNotFound
     */
    public function generateUri(string $name, array $params = [], array $queryParams = []): ?string
    {
        $generator = $this->routerContainer->getGenerator();
        $uri = $generator->generate($name, $params);
        if (!empty($queryParams)) {
            return $uri . '?' . http_build_query($queryParams);
        }
        return $uri;
    }
}
