<?php

namespace Framework\Router;

use Framework\Router;

/**
 * Class RouterTwigExtension
 * Contient les extension utilisé par le moteur de template TWIG
 * @package Framework\Router
 */
class RouterTwigExtension extends \Twig_Extension
{

    /**
     * Router utilisé
     * @var Router
     */
    private $router;

    /**
     * RouterTwigExtension constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Tableau recensant toutes les extensions TWIG définient après
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('path', [$this, 'pathFor'])
        ];
    }

    /**
     * Extension permet de récuérer l'uri d'une route
     * @param string $path
     * @param array $params
     * @return string
     */
    public function pathFor(string $path, array $params = []) : string
    {
        return $this->router->generateUri($path, $params);
    }
}
