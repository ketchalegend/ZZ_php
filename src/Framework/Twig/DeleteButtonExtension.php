<?php

namespace Framework\Twig;

use Framework\Middleware\CsrfMiddleware;
use Framework\Router;

class DeleteButtonExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var CsrfMiddleware
     */
    private $csrfMiddleware;

    public function __construct(Router $router, CsrfMiddleware $csrfMiddleware)
    {
        $this->router = $router;
        $this->csrfMiddleware = $csrfMiddleware;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('deleteButton', [$this,'deleteButton'], [
                'is_safe' => ['html'],
            ])
        ];
    }

    public function deleteButton(string $namespace, array $params, ?string $modalId = null)
    {
        $path = $this->router->generateUri($namespace, $params);

        $csrf = '<input type="hidden" name="' .
            $this->csrfMiddleware->getFormKey()  .
            '" value="' . $this->csrfMiddleware->generateToken() .
            '"/>';
        $modal = "";
        if ($modalId) {
            $modal = "data-toggle=\"modal\" data-target=\"#$modalId\"";
        }

        $html = "<form style=\"display: inline;\" action=\"$path\" method=\"POST\">
                        <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                        <button class=\"btn btn-danger deleteButton\" $modal>
                            <i class=\"fas fa-times text-white\"></i>
                        </button>
                        $csrf
                    </form>";

        return $html;
    }
}
