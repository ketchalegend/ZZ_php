<?php

namespace Framework\Twig;

use Framework\Session\FlashService;

class FlashExtension extends \Twig_Extension
{
    private $flashService;

    public function __construct(FlashService $flashService)
    {
        $this->flashService = $flashService;
    }

    public function getFunctions() :array
    {
        return [
            new \Twig_SimpleFunction('flash', [$this, 'getFlash']),
            new \Twig_SimpleFunction('showFlash', [$this, 'showFlash'], ['is_safe' => ['html']])
        ];
    }

    public function showFlash(?string $key = null)
    {
        if ($key === 'success' && $this->flashService->get($key)) {
            return "<div class=\"alert alert-success alert-dismissible fade show\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>" . $this->flashService->get($key) . "</div>";
        } elseif ($key === 'error' && $this->flashService->get($key)) {
            return "<div class=\"alert alert-danger\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button>" . $this->flashService->get($key) . "</div>";
        } else {
            $html = '';
            if ($this->flashService->get('success')) {
                $html .= "<div class=\"alert alert-success alert-dismissible fade show\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\">&times;</span>
                            </button>" . $this->flashService->get('success') . "</div>";
            }
            if ($this->flashService->get('error')) {
                $html .= "<div class=\"alert alert-danger alert-dismissible fade show\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                                <span aria-hidden=\"true\">&times;</span>
                            </button>" . $this->flashService->get('error') . "</div>";
            }
            return $html;
        }
    }

    public function getFlash($type) : ?string
    {
        return $this->flashService->get($type);
    }
}
