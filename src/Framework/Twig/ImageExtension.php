<?php

namespace Framework\Twig;

class ImageExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('thumb', [$this, 'thumb'])
        ];
    }

    public function thumb(string $logo, string $origns)
    {
        $extension = pathinfo($logo, PATHINFO_EXTENSION);
        $filename = pathinfo($logo, PATHINFO_FILENAME);
        return "/uploads/$origns/" . $filename . '_thumb.' . $extension;
    }
}
