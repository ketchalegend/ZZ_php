<?php

namespace Framework\Twig;

use App\Auth\DatabaseAuth;
use Framework\Middleware\CsrfMiddleware;
use Framework\Router;
use Psr\Container\ContainerInterface;

class NavExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var CsrfMiddleware
     */
    private $csrfMiddleware;

    public function __construct(
        Router $router,
        ContainerInterface $container,
        CsrfMiddleware $csrfMiddleware
    ) {
        $this->container = $container;
        $this->router = $router;
        $this->csrfMiddleware = $csrfMiddleware;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('nav', [$this,'nav'], [
                'is_safe' => ['html'],
            ])
        ];
    }

    public function nav($pagesKey)
    {
        $pages = $this->container->get($pagesKey);
        $html = "<nav class=\"navbar navbar-expand-md navbar-dark bg-dark\">
                    <a class=\"navbar-brand d-flex align-items-center\" href=\"/\">
                    <img src=\"/img/logo.png\" class=\"d-inline-block align-top mr-2\" alt=\"Logo du BDE\"
                         width=\"42\" height=\"42\">
                     BDE ZZ
                    </a>
        
                    <button class=\"navbar-toggler\" type=\"button\"
                            data-toggle=\"collapse\" data-target=\"#navbarCollapse\"
                            aria-controls=\"navbarCollapse\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                        <span class=\"navbar-toggler-icon\"></span>
                    </button>
        
                     <div class=\"collapse navbar-collapse\" id=\"navbarCollapse\">
                        <ul class=\"navbar-nav mr-auto\">";

        foreach ($pages as $page) {
            $html = $html . "<li class= \"nav-item\">
                                <a class=\"nav-link\" href=\"" . $page['link'] . "\">" . $page['name'] . "</a>
                            </li>";
        }

        $html = $html . "</ul>";

        $user = $this->container->get(DatabaseAuth::class)->getUser();

        $html = $html . "<div class=\"navbar-nav\">";
        if ($user) {
            $csrf = '<input type="hidden" name="' .
                $this->csrfMiddleware->getFormKey()  .
                '" value="' . $this->csrfMiddleware->generateToken() .
                '"/>';

            $username = $user->getNickname() ? $user->getNickname() : $user->firstname;

            $html = $html . "<a href=\"/espaceZZ\" class=\"navbar-text mr-2\">Bonjour, " . $username . "</a>
                <form action=\""
                . $this->router->generateUri('auth.logout') .
                "\" class=\"form-inline\" method=\"POST\">"
                . $csrf . "
                    <button class=\"btn btn-danger mx-md-2\">
                        <i class=\"fas fa-sign-out-alt text-white mr-2\"></i>Se déconnecter
                    </button>
                </form>";
        } else {
            $html = $html .
                "<div class=\"nav-item\">
                     <a class=\"btn btn-primary\" href=\"" . $this->router->generateUri('auth.login') . "\">
                        <i class=\"fas fa-sign-in-alt text-white mr-2\"></i>Se connecter
                    </a>
                </div>";
        }
        $html = $html . "</div>
                </div>
            </nav>";

        return $html;
    }
}
