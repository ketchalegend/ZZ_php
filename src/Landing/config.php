<?php

return [
    'landing.prefix' => '/',
    'public.nav' => [
        'home' => [
            'link' => '/',
            'name' => 'Acceuil'
        ]
    ]
];
