<?php

namespace App\Product\Entity;

class Product
{
    public $id;

    public $name;

    public $price;

    public $contributor_price;

    public $logo;

    public $visible;
}
