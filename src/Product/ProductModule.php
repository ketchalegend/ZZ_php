<?php

namespace App\Product;

use App\Club\Actions\ClubAction;
use App\Product\Actions\ProductAction;
use App\Product\Actions\ProductCrudAction;
use Framework\Router;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;

class ProductModule extends Module
{
    const DEFINITIONS = __DIR__ . '/config.php';
    const MIGRATIONS = __DIR__ . '/db/migrations';
    const SEEDS = __DIR__ . '/db/seeds';

    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath('product', __DIR__ . '/views');

        $router = $container->get(Router::class);
        $prefix = $container->get('product.prefix');

        $router->get($prefix, ProductAction::class, 'product.index');

        if ($container->has('admin.prefix')) {
            $adminPrefix = $container->get('admin.prefix');
            $router->crud("$adminPrefix/produits", ProductCrudAction::class, 'product.admin');
            $router->post("$adminPrefix/produits/activer/{id}", ProductAction::class, "product.admin.activate", [
                "id" => "\d+"
            ]);
        }
    }
}
