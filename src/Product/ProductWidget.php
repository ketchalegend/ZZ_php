<?php

namespace App\Product;

use App\Product\Table\ProductTable;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

class ProductWidget implements WidgetInterface
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var ProductTable
     */
    private $productTable;

    public function __construct(RendererInterface $renderer, ProductTable $productTable)
    {
        $this->renderer = $renderer;
        $this->productTable = $productTable;
    }

    public function render(): string
    {
        $products = $this->productTable->getProductsNumber();
        $productsVisible = $this->productTable->getVisibleProductsNumber();
        return $this->renderer->render('@product/admin/products/widget', compact('products', 'productsVisible'));
    }
}
