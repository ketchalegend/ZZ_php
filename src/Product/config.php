<?php

use App\Product\ProductWidget;

return [
    'product.prefix' => '/produits',
    'admin.widgets' => \DI\add([
        \DI\get(ProductWidget::class)
    ]),
    'public.nav' => \DI\add([
        'product' => [
            'link' => '/produits',
            'name' => 'Produits'
        ]
    ])
];
