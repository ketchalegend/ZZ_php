<?php


use Phinx\Migration\AbstractMigration;

class CreateProductTable extends AbstractMigration
{
    public function change()
    {
        $this->table('products')
            ->addColumn('name', 'string')
            ->addColumn('price', 'float')
            ->addColumn('contributor_price', 'float')
            ->addColumn('visible', 'boolean', [
                'default' => true
            ])
            ->addColumn('logo', 'string', ['null' => true])
            ->create();
    }
}
