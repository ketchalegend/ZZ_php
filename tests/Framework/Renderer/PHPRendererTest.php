<?php

namespace Tests\Framework;

use \PHPUnit\Framework\TestCase;
use Framework\Renderer\PHPRenderer;

class RendererTest extends TestCase
{
    private $renderer;

    public function setUp()
    {
        $this->renderer = new PHPRenderer(__DIR__ . '/views');
    }

    public function testRendererTheRightPath()
    {
        $this->renderer->addPath('blog', __DIR__ . '/views');
        $content = $this->renderer->render('@blog/demo');
        $this->assertEquals('Je suis la demo', $content);
    }

    public function testRendererTheDefaultPath()
    {
        $content = $this->renderer->render('demo');
        $this->assertEquals('Je suis la demo', $content);
    }

    public function testRendererWithParams()
    {
        $content = $this->renderer->render('demoparams', [
            'name' => 'John'
        ]);
        $this->assertEquals('Salut John', $content);
    }

    public function testGlobalParams()
    {
        $this->renderer->addGlobal('name', 'John');
        $content = $this->renderer->render('demoparams');
        $this->assertEquals('Salut John', $content);
    }
}
