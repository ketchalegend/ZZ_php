<?php

namespace Tests\Framework;

use Framework\Router;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\ServerRequest;

class RouterTest extends TestCase
{

    private $router;

    public function setUp()
    {
        $this->router = new Router();
    }

    public function testGetMethod()
    {
        $request = new ServerRequest('GET', '/blog');
        $this->router->get(
            '/blog',
            function () {
                return 'hello';
            },
            'blog'
        );
        $route = $this->router->match($request);
        $this->assertEquals('blog', $route->getName());
        $this->assertEquals('hello', call_user_func_array($route->getCallback(), [$request]));
    }

    public function testGetMethodIfURLDoesNotExist()
    {
        $request = new ServerRequest('GET', 'blog');
        $this->router->get(
            '/jenexistepas',
            function () {
                return 'hello';
            },
            'blog'
        );
        $route = $this->router->match($request);
        $this->assertEquals(null, $route);
    }

    public function testGetMethodWithParams()
    {
        $request = new ServerRequest('GET', '/blog/slug-4');
        $this->router->get(
            '/blog',
            function () {
                return 'jesuisnimportequoi';
            },
            'posts'
        );
        $this->router->get(
            '/blog/{slug}-{id}',
            function () {
                return 'hello';
            },
            'post.show',
            [
                "slug" => "[a-z0-9\-]+",
                "id" => "\d+"
            ]
        );
        $route = $this->router->match($request);
        $this->assertEquals('post.show', $route->getName());
        $this->assertEquals('hello', call_user_func_array($route->getCallback(), [$request]));
        $this->assertEquals(['slug' => 'slug', 'id' => '4'], $route->getParams());
        // Test invalid URL
        $route = $this->router->match(new ServerRequest('GET', '/blog/sl_ug-4'));
        $this->assertEquals(null, $route);
    }

    public function testGenerateUri()
    {
        $this->router->get(
            '/blog',
            function () {
                return 'jesuisnimportequoi';
            },
            'posts'
        );
        $this->router->get(
            '/blog/{slug}-{id}',
            function () {
                return 'hello';
            },
            'post.show',
            [
                "slug" => "[a-z0-9\-]+",
                "id" => "\d+"
            ]
        );
        $uri = $this->router->generateUri('post.show', ['slug' => 'slug', 'id' => 4]);
        $this->assertEquals('/blog/slug-4', $uri);
    }

    public function testGenerateUriWithQueryParams()
    {
        $this->router->get(
            '/blog',
            function () {
                return 'jesuisnimportequoi';
            },
            'posts'
        );
        $this->router->get(
            '/blog/{slug}-{id}',
            function () {
                return 'hello';
            },
            'post.show',
            [
                "slug" => "[a-z0-9\-]+",
                "id" => "\d+"
            ]
        );
        $uri = $this->router->generateUri(
            'post.show',
            ['slug' => 'slug', 'id' => 4],
            ['p' => 2]
        );
        $this->assertEquals('/blog/slug-4?p=2', $uri);
    }
}
